# Open Data Model

Connecting the dots to have an open data exchange ecosystem

## A challenge   
Nowadays many organizations posess data about us. Sometimes we, as a person, would like to do something with that data. Fortunately, more and more possibilities are offered so you can download your data by yourself and place it in your own wallet. So far so good. But now things get complicated quickly. Suppose you want to reuse your downloaded data, to be able to subscribe to a service or close a contract. The required data is often requested in a format that differs from the format in which you stored your data. How are we going to translate back and forth?

## A solution   
What if data providing services could describe the data format they offer; data consuming services could describe what data format they expect, and we together define the business rules needed to translate between the data formats? Then this challenge would be solved.

Easier said then done. So, let's do it then. 


### The goal:
- connect organizations and individuals and work together to create a shared open data model
- lean as much as possible on existing technologies and standards
- keep it simple and easy to understand and implement
- describe data at an elementary, attribute based level
- group attributes of certain types, for example name and birthdate are 'personal', where Sesamestreet is part of an 'address' 
- map attributes from data providing services
- have business rules for processing these attributes
- map attributes to a model which data consuming services can process
- input, processing and output is testable on different levels

A visual representation of the challenge:   

![input-processing-output](input-processing-output.png "Input - processing - output example")

**Input**   
These are data providing services / data sources.

**Processing**   
Attributes are translated to the required format by applying certain business rules. Things like renaming, formatting, splitting, calculating and so on. Business rules are described in an easily understandable pseudo code. From there on implementations can be made in many languages.

**Output**   
Data is output in the requested format (think xml, json) and structure (think hierachy) which forms the Open Data Model. This is then used as input for a data consuming service.
